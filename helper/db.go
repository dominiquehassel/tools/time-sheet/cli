package helper

import "database/sql"

type DbStore struct{
	DB *sql.DB
}


func (s *DbStore)Execute( action func (*sql.Tx) (error)) error{
	tx, err := s.DB.Begin()
	if err != nil {
		return err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()
	err = action(tx)
	return err
}