package project_test

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
)


func TestCreateNewService(t *testing.T){
	// perform
	s := project.NewService(&project.MockStore{})
	// assert
	assert.NotNil(t, s, "Service should not be nil")
}

func TestAddNewProject(t *testing.T){
	// prepare
	name := "test"
	description := "description"
	bookingReference := "br"
	mockStore := &project.MockStore{}
	mockStore.On("Save", mock.Anything).Return(nil)
	// perform
	s := project.NewService(mockStore)
	p, err := s.AddProject(name,description,bookingReference)
	// assert
	assert.NotNil(t, p, "Project should be non empty")
	assert.NoError(t, err, "Should be no error")
	assert.Equal(t, name, p.Name, "Name should be equal")
	assert.Equal(t, description, p.Description, "Name should be equal")
	assert.Equal(t, bookingReference, p.BookingReference, "Name should be equal")
}

func TestAddNewProjectWithError(t *testing.T){
	// prepare
	name := ""
	description := "description"
	bookingReference := "br"
	mockStore := &project.MockStore{}
	mockStore.On("Save", mock.Anything).Return(nil)
	// perform
	s := project.NewService(mockStore)
	p, err := s.AddProject(name,description,bookingReference)
	// assert
	assert.Nil(t, p, "Project should be empty")
	assert.EqualError(t, err, "Name should not be empty", "Should be no error")
}


func TestAddNewProjectWithErrorOnSave(t *testing.T){
	// prepare
	name := "name"
	description := "description"
	bookingReference := "br"
	mockStore := &project.MockStore{}
	mockStore.On("Save", mock.Anything).Return(fmt.Errorf("Failed on Save"))
	// perform
	s := project.NewService(mockStore)
	p, err := s.AddProject(name,description,bookingReference)
	// assert
	assert.Nil(t, p, "Project should be empty")
	assert.EqualError(t, err, "Failed on Save", "Should be no error")
}


func TestUpdateExistingProjectWithNewName(t *testing.T){
	// prepare
	name :="newName"
	p, _ := project.New("test", "test", "test")
	mockStore := &project.MockStore{}
	mockStore.On("Get", p.ID).Return(p, nil)
	mockStore.On("Update", mock.Anything).Return(nil)
	// perform
	s := project.NewService(mockStore)
	err := s.UpdateProject(p.ID, name, "", "")
	// assert
	assert.NoError(t, err)
	assert.Equal(t, name, p.Name)
	assert.NotEqual(t, "", p.Description)
	assert.NotEqual(t, "", p.BookingReference)
}

func TestUpdateProjectWithProjectNotFound(t *testing.T) {
	// prepare
	p, _ := project.New("test", "test", "test")
	mockStore := &project.MockStore{}
	mockStore.On("Get", p.ID).Return(nil, nil)
	// perform
	s := project.NewService(mockStore)
	err := s.UpdateProject(p.ID, "test", "", "")
	// assert
	assert.EqualError(t, err, fmt.Sprintf("No project found with the id: %s", p.ID))
}


func TestFindProjects(t *testing.T){
	// prepare
	name := "test"
	mockStore := &project.MockStore{}
	p1,_:=project.New("test", "description", "bookingReference")
	p2,_:=project.New("test", "other description", "other bookingReference")
	mockStore.On("Find", name).Return([]*project.Project{p1, p2}, nil)
	// perform
	s := project.NewService(mockStore)
	ps, err := s.FindProjects(name)
	assert.NoError(t, err)
	assert.NotEmpty(t, ps)
	assert.Equal(t, 2, len(ps))
}


func TestFindProjectsWithEmptyResults(t *testing.T){
	// prepare
	name := "test"
	mockStore := &project.MockStore{}
	mockStore.On("Find", name).Return([]*project.Project{}, nil)
	// perform
	s := project.NewService(mockStore)
	ps, err := s.FindProjects(name)
	assert.NoError(t, err)
	assert.Empty(t, ps)
	assert.Equal(t, 0, len(ps))
}

func TestFindProjectWithError(t *testing.T){
	// prepare
	name := "test"
	mockStore := &project.MockStore{}
	mockStore.On("Find", name).Return(nil, fmt.Errorf("Some error"))
	// perform
	s := project.NewService(mockStore)
	ps, err := s.FindProjects(name)
	assert.EqualError(t, err,"Some error")
	assert.Empty(t, ps)
	assert.Equal(t, 0, len(ps))
}


func TestDeleteProject(t *testing.T){
	// prepare
	id := uuid.New()
	mockStore := &project.MockStore{}
	mockStore.On("Delete", id).Return(nil)
	// perform
	s := project.NewService(mockStore)
	err := s.DeleteProject(id)
	assert.NoError(t, err)
}

func TestDeleteProjectWithError(t *testing.T){
	// prepare
	id := uuid.New()
	mockStore := &project.MockStore{}
	mockStore.On("Delete", id).Return(fmt.Errorf("Some error"))
	// perform
	s := project.NewService(mockStore)
	err := s.DeleteProject(id)
	assert.EqualError(t, err, "Some error")
}