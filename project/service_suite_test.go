package project_test

import (
	"fmt"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/migration"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
)

type projectSuiteTest struct {
	migration.PostgresqlSuite
}

func TestProjectSuite(t *testing.T) {

	dsn := os.Getenv("POSTGRES_SQL_TEST_URL")
	if dsn == "" {
		dsn = fmt.Sprintf("postgres://%s:%s@%s:%d/postgres?sslmode=disable", migration.User, migration.Password, migration.Host, migration.Port)
		/*fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbName)*/
	}

	projectSuite := &projectSuiteTest{
		migration.PostgresqlSuite{
			DSN:                     dsn,
			MigrationLocationFolder: "../migration/data",
		},
	}

	suite.Run(t, projectSuite)
}

func (s *projectSuiteTest) TestAddingProject() {

	// prepare
	store := project.NewPgStore(s.DBConn)
	service := project.NewService(store)

	// run Test Suite
	s.T().Run("should add a new project", func(t *testing.T) {
		_, err := service.AddProject("name", "description", "bookingReference")
		assert.NoError(t, err)
	})
}

func (s *projectSuiteTest) TestUpdateExistingProject() {
	// prepare
	store := project.NewPgStore(s.DBConn)
	service := project.NewService(store)

	// run Test Suite
	s.T().Run("should update an existing project", func(t *testing.T) {
		p, err := service.AddProject("name", "description", "bookingReference")
		err = service.UpdateProject(p.ID, "newName", "newDescription", "bookingReference")
		assert.NoError(t, err)
		p2, _ := store.Get(p.ID)
		assert.Equal(t, "newName", p2.Name)
		assert.Equal(t, "newDescription", p2.Description)
	})
}

func (s *projectSuiteTest) TestFindTwoExistingProjects() {
	// prepare
	store := project.NewPgStore(s.DBConn)
	service := project.NewService(store)

	// run Test Suite
	s.T().Run("should return a list of 2 projects", func(t *testing.T) {
		p1, err := service.AddProject("name", "description", "bookingReference")
		assert.NoError(t, err)
		p2, err := service.AddProject("name", "other description", "other bookingReference")
		assert.NoError(t, err)
		_, err = service.AddProject("other name", "description", "bookingReference")
		assert.NoError(t, err)
		ps, err := service.FindProjects("name")
		assert.NoError(t, err)
		assert.NotEmpty(t, ps)
		assert.Equal(t, 2, len(ps))
		assert.Contains(t, ps, p1)
		assert.Contains(t, ps, p2)
	})
}

func (s *projectSuiteTest) TestFindProjectWithEmptyResult() {
	// prepare
	store := project.NewPgStore(s.DBConn)
	service := project.NewService(store)

	// run Test Suite
	s.T().Run("should return an empty list of projects", func(t *testing.T) {
		_, err := service.AddProject("name", "description", "bookingReference")
		assert.NoError(t, err)
		_, err = service.AddProject("name", "other description", "other bookingReference")
		assert.NoError(t, err)
		_, err = service.AddProject("other name", "description", "bookingReference")
		assert.NoError(t, err)
		ps, err := service.FindProjects("NotExists")
		assert.NoError(t, err)
		assert.Empty(t, ps)
		assert.Equal(t, 0, len(ps))
	})
}

func (s *projectSuiteTest) TestDeleteExistingProject() {
	// prepare
	store := project.NewPgStore(s.DBConn)
	service := project.NewService(store)

	// run Test Suite
	s.T().Run("should delete an existing project", func(t *testing.T) {
		p, err := service.AddProject("name", "description", "bookingReference")
		assert.NoError(t, err)
		_, err = service.AddProject("name", "other description", "other bookingReference")
		assert.NoError(t, err)
		_, err = service.AddProject("other name", "description", "bookingReference")
		assert.NoError(t, err)
		err = service.DeleteProject(p.ID)
		assert.NoError(t, err)
	})
}

func (s *projectSuiteTest) TestDeleteNonExistingProject() {
	// prepare
	store := project.NewPgStore(s.DBConn)
	service := project.NewService(store)

	// run Test Suite
	s.T().Run("should delete an existing project", func(t *testing.T) {
		_, err := service.AddProject("name", "description", "bookingReference")
		assert.NoError(t, err)
		_, err = service.AddProject("name", "other description", "other bookingReference")
		assert.NoError(t, err)
		_, err = service.AddProject("other name", "description", "bookingReference")
		assert.NoError(t, err)
		err = service.DeleteProject(uuid.New())
		assert.NoError(t, err)
	})
}
