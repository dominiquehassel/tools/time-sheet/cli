package project

import (
	"fmt"

	"github.com/google/uuid"
)

// Project consists of a name, a description and a booking reference.
// Each project should have an autogenerated uuid for unique identification.
// Therefor a project should only be created via New(Name, Description, BookingReference)
type Project struct {
	ID uuid.UUID
	Name string
	Description string
	BookingReference string
}


// New creates a new project with a name, a description and a booking reference.
// The ID of the project will be automatically created.
// The name and booking reference will be validated for 'non empty'
func New(name string, description string, bookingReference string) (*Project, error) {
	if name == "" {
		return nil, fmt.Errorf("Name should not be empty")
	}
	if bookingReference == "" {
		return nil, fmt.Errorf("Booking reference should not be empty")
	}
	p := &Project{ID: uuid.New(), Name: name, Description: description, BookingReference: bookingReference}
	return p, nil
}
