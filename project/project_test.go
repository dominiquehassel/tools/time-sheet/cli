package project_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
)


func TestNewProjectWithAllParametersFilled(t *testing.T){
	// prepare
	name := "testName"
	description := "testDescription"
	bookingReference := "testReference"
	// perform
	p, err :=project.New(name,description,bookingReference)
	// assert
	assert.NotNil(t, p, "Project should not be empty!")
	assert.NoError(t, err, "Error should be nil.")
}
	
func TestNewProjectWithEmptyName(t *testing.T){
	// prepare
	name := ""
	description := "testDescription"
	bookingReference := "testReference"
	// perform
	p, err := project.New(name, description, bookingReference)
	// assert
	assert.Nil(t, p, "Project should be empty!")
	assert.EqualError(t, err, "Name should not be empty", "Error should not be empty")
}

func TestNewProjectWithEmptyBookingReference(t *testing.T){
	// prepare
	name := "testName"
	description := "testDescription"
	bookingReference := ""
	// perform
	p, err :=project.New(name,description,bookingReference)
	// assert
	assert.Nil(t, p, "Project should be empty!")
	assert.EqualError(t, err, "Booking reference should not be empty", "Error should not be empty")
}