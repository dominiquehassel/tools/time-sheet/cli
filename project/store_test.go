package project_test

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
)


func TestExecuteSimpleQuery(t *testing.T){
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	p, _ := project.New("name", "description", "bookingReference")
	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO project").WithArgs(p.ID, p.Name, p.Description, p.BookingReference).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()
	// perform
	s := project.NewPgStore(db)
	err = s.Save(p)
	// assert
	assert.NoError(t, err, "Save should not result in an error")
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestExecuteSimpleQueryWillResultInError(t *testing.T){
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	p, _ := project.New("name", "description", "bookingReference")
	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO project").WithArgs(p.ID, p.Name, p.Description, p.BookingReference).WillReturnError(fmt.Errorf("ID already exists"))
	mock.ExpectRollback()
	// perform
	s := project.NewPgStore(db)
	err = s.Save(p)
	// assert
	assert.EqualError(t, err, "ID already exists", "Save should result in an error")
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
}


func TestGetProjectWithId(t *testing.T){
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	p, _ := project.New("name", "description", "bookingReference")
	mockedRows := sqlmock.NewRows([]string{"id", "name", "description", "bookingReference"}).AddRow(p.ID, p.Name, p.Description, p.BookingReference)
	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM project where id=$1")).WithArgs(p.ID).WillReturnRows(mockedRows)
	mock.ExpectCommit()
	// perform
	s := project.NewPgStore(db)
	r, err := s.Get(p.ID)
	// assert
	assert.NoError(t, err)
	assert.Equal(t, p, r)
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestGetProjectNotFound(t *testing.T){
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	id := uuid.New()
	mockedRows := sqlmock.NewRows([]string{"id", "name", "description", "bookingReference"})
	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM project where id=$1")).WithArgs(id).WillReturnRows(mockedRows)
	mock.ExpectCommit()
	// perform
	s := project.NewPgStore(db)
	r, err := s.Get(id)
	// assert
	assert.NoError(t, err)
	assert.Nil(t, r, "Should be empty")
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
} 


func TestUpdateExistingProject(t *testing.T){
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	p, _ := project.New("name", "description", "bookingReference")
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("UPDATE project set projectName=$1, projectDescription=$2, bookingReference=$3 where id=$4")).WithArgs(p.Name, p.Description, p.BookingReference, p.ID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()
	// perform
	s := project.NewPgStore(db)
	err = s.Update(p)
	// assert
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestFindExistingProjects(t *testing.T){
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	name := "test"
	p1, _ := project.New(name, "other description", "other bookingReference")
	p2, _ := project.New(name, "description", "bookingReference")
	mockedRows := sqlmock.NewRows([]string{"id", "name", "description", "bookingReference"})
	mockedRows.AddRow(p1.ID, p1.Name, p1.Description, p1.BookingReference)
	mockedRows.AddRow(p2.ID, p2.Name, p2.Description, p2.BookingReference)
	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM project where projectName=$1")).WithArgs(name).WillReturnRows(mockedRows)
	mock.ExpectCommit()
	// perform
	s := project.NewPgStore(db)
	ps, err := s.Find(name)
	// assert
	assert.NotEmpty(t, ps)
	assert.Equal(t, 2, len(ps))
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
}


func TestDeleteExistingProject(t *testing.T){
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	id := uuid.New()
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("DELETE FROM project where id=$1")).WithArgs(id).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()
	// perform
	s := project.NewPgStore(db)
	err = s.Delete(id)
	// assert
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
}