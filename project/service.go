package project

import (
	"fmt"

	"github.com/google/uuid"
)

// Service provides the functionality to add new project and  change or delete existing ones.
type Service struct {
	store Store
}

// NewService creates a new service with a connectd store
func NewService(store Store) *Service {
	return &Service{store: store}
}

// AddProject creates a new project and adds it to the persistance layer.
func (s *Service) AddProject(name string, description string, bookingReference string) (*Project, error){
	p, err := New(name, description, bookingReference)
	if err != nil {
		return nil, err
	}
	err = s.store.Save(p)
	if err != nil {
		return nil, err
	}
	return p, nil
}

// UpdateProject updates the project with the new values for the given id. 
// If no Project was found for the if, an error will be thrown.
// Values will only be updated if they are not empty.
func (s *Service) UpdateProject(id uuid.UUID, name string, description string, bookingReference string) error {
	p, err := s.store.Get(id)
	if err != nil {
		return err
	}
	if p == nil {
		return fmt.Errorf("No project found with the id: %s", id)
	}
	if name != "" {
		p.Name = name
	}
	if description != "" {
		p.Description = description
	}
	if bookingReference != "" {
		p.BookingReference = bookingReference
	}
	return s.store.Update(p)
}

// FindProjects searches for projects with the given name and returns a list of all hits or an empty list.
func (s *Service) FindProjects(name string)([]*Project, error){
	ps, err := s.store.Find(name)
	if err != nil {
		return ps, err
	}
	return ps, nil
}

// DeleteProject deletes the project with the given id. If there is noch such project nothing happens.
func (s *Service) DeleteProject(id uuid.UUID) error {
	return s.store.Delete(id)
}