package project

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
)


type MockStore struct {
	mock.Mock
}

func (s *MockStore) Save(project *Project) error{
	args := s.Called(project)
	return args.Error(0)
}

func (s *MockStore) Update(p *Project) error {
	args := s.Called(p)
	return args.Error(0)
}

func (s *MockStore) Get(id uuid.UUID) (*Project, error) {
	args := s.Called(id)
	if args.Get(0) == nil{
		return nil,  args.Error(1)
	}
	return args.Get(0).(*Project),  args.Error(1)
}

func (s *MockStore) Find(name string) ([]*Project, error){
	args := s.Called(name)
	if args.Get(0) == nil {
		return []*Project{}, args.Error(1)
	}
	return args.Get(0).([]*Project),  args.Error(1)
}

func (s *MockStore) Delete(id uuid.UUID) error {
	args := s.Called(id)
	return args.Error(0)
}