package project

import (
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/helper"
)

// Store defines the interface actions for the persitance layer.
// TODO: Better documention.
type Store interface {
	Save(*Project) error
	Update(*Project) error
	Get(uuid.UUID) (*Project, error)
	Find(name string) ([]*Project, error)
	Delete(uuid.UUID) (error)
} 

// PgStore is an implementation of the #Store for Postgresql Database connections.
type PgStore struct {
	helper.DbStore
}

// NewPgStore creates a new Store with the given sql connection.
// This store is optimized for Postgresql DB
func NewPgStore(db *sql.DB) *PgStore{
	s :=  &PgStore{}
	s.DB = db
	return s
}


// Save `s the projects data into the connectad database.
// The given project must be non nil.
func (s *PgStore) Save(project *Project) error {
	return s.Execute(func (tx *sql.Tx) (error) {
		if _, err := tx.Exec("INSERT INTO project (id, projectName, projectDescription, bookingReference) VALUES ($1, $2, $3, $4)", project.ID, project.Name, project.Description, project.BookingReference); err != nil {
			return err
		}
		return nil
	})
}

// Update saves the changed project definition to the connected database.
func (s *PgStore) Update(p *Project) error {
	return s.Execute(func (tx *sql.Tx) (error) {
		if _, err := tx.Exec("UPDATE project set projectName=$1, projectDescription=$2, bookingReference=$3 where id=$4", p.Name, p.Description, p.BookingReference, p.ID); err != nil {
			return err
		}
		return nil
	})
}

// Get returns the Project for the given id. If the project could not be found, nil will be returned.
func (s *PgStore) Get(id uuid.UUID) (*Project, error) {
	p := &Project{}
	err :=  s.Execute(func (tx *sql.Tx) (error) {
		// execute the sql statement
		row := tx.QueryRow("SELECT * FROM project where id=$1", id)

		// unmarshal the row object to user
		err := row.Scan(&p.ID, &p.Name, &p.Description, &p.BookingReference)
		if err == sql.ErrNoRows {
			p = nil
			return nil
		} else if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return p, nil
}

// Find searches for projects with the given name and returns a list of all hits
func (s *PgStore) Find(name string) ([]*Project, error) {
	var projects []*Project
	err :=  s.Execute(func (tx *sql.Tx) (error) {
		// execute the sql statement
		rows, err := tx.Query("SELECT * FROM project where projectName=$1", name)
		if err != nil {
			return err
		}
		for rows.Next(){ 
			// unmarshal the row object to user
			p := &Project{}
			err := rows.Scan(&p.ID, &p.Name, &p.Description, &p.BookingReference)
			if err == sql.ErrNoRows {
				return nil
			} else if err != nil {
				return err
			}
			projects = append(projects, p)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return projects, nil
}

// Delete drops a row with the given uuid.
// If the row does not exists nothing happens.
func (s *PgStore) Delete(id uuid.UUID) error {
	return s.Execute(func (tx *sql.Tx) (error) {
		if _, err := tx.Exec("DELETE FROM project where id=$1", id); err != nil {
			return err
		}
		return nil
	})
}



