module gitlab.com/dominiquehassel/tools/time-sheet/cli

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-openapi/strfmt v0.19.11 // indirect
	github.com/golang-migrate/migrate/v4 v4.13.0
	github.com/google/uuid v1.1.2
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/lib/pq v1.8.0
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
)
