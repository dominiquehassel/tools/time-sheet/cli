.PHONY: dependency setup-test  format unit-test integration-test docker-test-up docker-test-down docker-up docker-down clear 

dependency:
	@go get -v ./...

setup-test:
	@export PSQL_TEST_URL="host=localhost port=5432 user=postgres password=changeme dbname=postgres"   

integration-test: docker-test-up dependency 
	@go test -v ./...

unit-test: dependency
	@go test -v -short ./...

format: dependency
	@go fmt ./...
	@go vet ./...

docker-test-up:
	@docker-compose -f docker-compose-test.yaml up -d

docker-test-down:
	@docker-compose -f docker-compose-test.yaml down
	
docker-up: 
	@docker-compose up -d

docker-down:
	@docker-compose down

clear: docker-down