package session_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

func TestCreateNewService(t *testing.T) {
	// perform
	s := session.NewService(&session.MockStore{}, &project.MockStore{})
	// assert
	assert.NotNil(t, s, "Service should not be nil")
}

func TestAddNewSession(t *testing.T) {
	// prepare
	date := time.Now()
	duration := 4.5
	projectID := uuid.New()
	mockProjectStore := &project.MockStore{}
	mockProjectStore.On("Get", projectID).Return(&project.Project{ID: projectID, Name: "test", Description: "test", BookingReference: "test"}, nil)
	mockStore := &session.MockStore{}
	mockStore.On("Save", mock.Anything).Return(nil)
	s := session.NewService(mockStore, mockProjectStore)
	//perform
	newSession, err := s.AddSession(date, duration, projectID)
	// assert
	assert.NoError(t, err)
	assert.Equal(t, date, newSession.Date)
	assert.Equal(t, duration, newSession.Duration)
	assert.Equal(t, projectID, newSession.ProjectID)
}

func TestAddNewSessionWithNoProjectFound(t *testing.T) {
	// prepare
	date := time.Now()
	duration := 4.5
	projectID := uuid.New()
	mockProjectStore := &project.MockStore{}
	mockProjectStore.On("Get", projectID).Return(nil, nil)
	mockStore := &session.MockStore{}
	s := session.NewService(mockStore, mockProjectStore)
	//perform
	_, err := s.AddSession(date, duration, projectID)
	// assert
	assert.EqualError(t, err, fmt.Sprintf("The project with the id '%s' could not be found. You can only create sessions for existing projects", projectID))
}

func TestAddNewSessionWithErrorFromGetProject(t *testing.T) {
	// prepare
	date := time.Now()
	duration := 4.5
	projectID := uuid.New()
	mockProjectStore := &project.MockStore{}
	mockProjectStore.On("Get", projectID).Return(nil, fmt.Errorf("Some Error"))
	mockStore := &session.MockStore{}
	s := session.NewService(mockStore, mockProjectStore)
	//perform
	_, err := s.AddSession(date, duration, projectID)
	// assert
	assert.EqualError(t, err, "Some Error")
}

func TestAddNewSessionWithErrorFromSave(t *testing.T) {
	// prepare
	date := time.Now()
	duration := 4.5
	projectID := uuid.New()
	mockProjectStore := &project.MockStore{}
	mockProjectStore.On("Get", projectID).Return(&project.Project{ID: projectID, Name: "test", Description: "test", BookingReference: "test"}, nil)
	mockStore := &session.MockStore{}
	mockStore.On("Save", mock.Anything).Return(fmt.Errorf("Some Error"))
	s := session.NewService(mockStore, mockProjectStore)
	//perform
	_, err := s.AddSession(date, duration, projectID)
	// assert
	assert.EqualError(t, err, "Some Error")
}

func TestUpdateExistingSession(t *testing.T) {
	// prepare
	expected, _ := session.New(time.Now(), 4.5, uuid.New())
	mockProjectStore := &project.MockStore{}
	mockProjectStore.On("Get", expected.ProjectID).Return(&project.Project{ID: expected.ProjectID, Name: "test", Description: "test", BookingReference: "test"}, nil)
	mockStore := &session.MockStore{}
	mockStore.On("Get", expected.ID).Return(expected, nil)
	mockStore.On("Update", expected).Return(nil)
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	err := s.UpdateSession(expected.ID, &expected.Date, expected.Duration, &expected.ProjectID)
	// assert
	assert.NoError(t, err)
}

func TestUpdateExistingSessionWithOnlyDuration(t *testing.T) {
	// prepare
	newDuration := 5.5
	existing, _ := session.New(time.Now(), 4.5, uuid.New())
	mockProjectStore := &project.MockStore{}
	mockProjectStore.On("Get", existing.ProjectID).Return(&project.Project{ID: existing.ProjectID, Name: "test", Description: "test", BookingReference: "test"}, nil)
	mockStore := &session.MockStore{}
	mockStore.On("Get", existing.ID).Return(existing, nil)
	mockStore.On("Update", &session.Session{existing.ID, existing.Date, newDuration, existing.ProjectID}).Return(nil)
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	err := s.UpdateSession(existing.ID, &existing.Date, newDuration, &existing.ProjectID)
	// assert
	assert.NoError(t, err)
	mockStore.AssertExpectations(t)
}

func TestFindSessionsByDateWithEmptyResult(t *testing.T) {
	// prepare
	from, _ := time.Parse("02-01-2006", "01-01-2020")
	to, _ := time.Parse("02-01-2006", "02-01-2020")
	mockProjectStore := &project.MockStore{}
	mockStore := &session.MockStore{}
	mockStore.On("FindByDate", from, to).Return([]*session.VSession{}, nil)
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	sessions, err := s.FindSessionsByDate(from, to)
	// assert
	assert.NoError(t, err)
	assert.Empty(t, sessions)
}

func TestFindSessionsByDateWithTwoSessionsFound(t *testing.T) {
	// prepare
	from, _ := time.Parse("02-01-2006", "01-01-2020")
	to, _ := time.Parse("02-01-2006", "02-01-2020")
	mockProjectStore := &project.MockStore{}
	mockStore := &session.MockStore{}
	s1, _ := session.NewVSession(from, 4.5, "test")
	s2, _ := session.NewVSession(to, 5.5, "test")
	mockStore.On("FindByDate", from, to).Return([]*session.VSession{s1, s2}, nil)
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	sessions, err := s.FindSessionsByDate(from, to)
	// assert
	assert.NoError(t, err)
	assert.NotEmpty(t, sessions)
	assert.Equal(t, 2, len(sessions))
}

func TestFindSessionByDateShouldFailBecauseToIsBeforeFrom(t *testing.T) {
	// prepare
	from, _ := time.Parse("02-01-2006", "01-01-2020")
	to, _ := time.Parse("02-01-2006", "02-01-2020")
	mockProjectStore := &project.MockStore{}
	mockStore := &session.MockStore{}
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	_, err := s.FindSessionsByDate(to, from)
	// assert
	assert.EqualError(t, err, "From date is behind to date")
}

func TestFindSessionsByDateWithErrorOnFindSessions(t *testing.T) {
	// prepare
	from, _ := time.Parse("02-01-2006", "01-01-2020")
	to, _ := time.Parse("02-01-2006", "02-01-2020")
	mockProjectStore := &project.MockStore{}
	mockStore := &session.MockStore{}
	mockStore.On("FindByDate", from, to).Return(nil, fmt.Errorf("Some error"))
	s := session.NewService(mockStore, mockProjectStore)
	// prefrom
	_, err := s.FindSessionsByDate(from, to)
	// assert
	assert.EqualError(t, err, "Some error")
}

func TestFindSessionsByDateWithProjectWithTwoSessionsFound(t *testing.T) {
	// prepare
	from, _ := time.Parse("02-01-2006", "01-01-2020")
	to, _ := time.Parse("02-01-2006", "02-01-2020")
	id := uuid.New()
	name := "test"
	mockProjectStore := &project.MockStore{}
	mockProjectStore.On("Get", id).Return(&project.Project{ID: id, Name: name, Description: "test", BookingReference: "test"}, nil)
	mockStore := &session.MockStore{}
	s1, _ := session.NewVSession(from, 4.5, name)
	s2, _ := session.NewVSession(to, 5.5, name)
	mockStore.On("FindByDateWithProject", from, to, name).Return([]*session.VSession{s1, s2}, nil)
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	sessions, err := s.FindSessionsByDateWithProject(from, to, name)
	// assert
	assert.NoError(t, err)
	assert.NotEmpty(t, sessions)
	assert.Equal(t, 2, len(sessions))
}

func TestDeleteSessionWithNoError(t *testing.T) {
	// preprare
	id := uuid.New()
	mockProjectStore := &project.MockStore{}
	mockStore := &session.MockStore{}
	mockStore.On("Delete", id).Return(nil)
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	err := s.DeleteSession(id)
	// assert
	assert.NoError(t, err)
	mockStore.AssertCalled(t, "Delete", id)
}
func TestDeleteSessionWithError(t *testing.T) {
	// preprare
	id := uuid.New()
	mockProjectStore := &project.MockStore{}
	mockStore := &session.MockStore{}
	mockStore.On("Delete", id).Return(fmt.Errorf("Some error"))
	s := session.NewService(mockStore, mockProjectStore)
	// perform
	err := s.DeleteSession(id)
	// assert
	assert.EqualError(t, err, "Some error")
	mockStore.AssertCalled(t, "Delete", id)
}
