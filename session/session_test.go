package session_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

func TestCreateNewSession(t *testing.T){
	// prepare
	date := time.Now()
	projectID, _ := uuid.Parse("5b1935c0-bf6a-4a66-ab49-52bdf76fed9f")
	duration := 4.5
	//perform
	s, err := session.New(date, duration, projectID)
	// assert
	assert.NoError(t, err)
	assert.Equal(t, duration, s.Duration)
	assert.Equal(t, projectID, s.ProjectID)
}

func TestCreateNewSessionNoDuration(t *testing.T){
	// prepare
	date := time.Now()
	projectID, _ := uuid.Parse("5b1935c0-bf6a-4a66-ab49-52bdf76fed9f")
	duration := 0.0
	//perform
	_, err := session.New(date, duration, projectID)
	// assert
	assert.EqualError(t, err, "A session withour a duration does not make any sense")
}