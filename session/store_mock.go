package session

import (
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
)

type MockStore struct {
	mock.Mock
}

func (s *MockStore) Save(session *Session) error {
	args := s.Called(session)
	return args.Error(0)
}

func (s *MockStore) Update(session *Session) error {
	args := s.Called(session)
	return args.Error(0)
}

func (s *MockStore) Get(id uuid.UUID) (*Session, error) {
	args := s.Called(id)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*Session), args.Error(1)
}

func (s *MockStore) FindByDate(from time.Time, to time.Time) ([]*VSession, error) {
	args := s.Called(from, to)
	if args.Get(0) == nil {
		return []*VSession{}, args.Error(1)
	}
	return args.Get(0).([]*VSession), args.Error(1)
}

func (s *MockStore) FindByDateWithProject(from time.Time, to time.Time, project string) ([]*VSession, error) {
	args := s.Called(from, to, project)
	if args.Get(0) == nil {
		return []*VSession{}, args.Error(1)
	}
	return args.Get(0).([]*VSession), args.Error(1)
}

func (s *MockStore) Delete(id uuid.UUID) error {
	args := s.Called(id)
	return args.Error(0)
}
