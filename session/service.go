package session

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
)

// Service provides the functionality to add new session and change or delete existing ones.
type Service struct {
	sessionStore Store
	projectStore project.Store
}

// NewService creates a new service with a connectd store
func NewService(sessionStore Store, projectStore project.Store) *Service {
	return &Service{sessionStore: sessionStore, projectStore: projectStore}
}

// AddSession creates a new Session and stores it in the persistance layer
func (s *Service) AddSession(date time.Time, duration float64, projectID uuid.UUID) (*Session, error) {
	p, err := s.projectStore.Get(projectID)
	if err != nil {
		return nil, err
	}
	if p == nil {
		return nil, fmt.Errorf("The project with the id '%s' could not be found. You can only create sessions for existing projects", projectID)
	}
	newSession, err := New(date, duration, projectID)
	if err != nil {
		return nil, err
	}
	err = s.sessionStore.Save(newSession)
	if err != nil {
		return nil, err
	}
	return newSession, nil
}

// UpdateSession tries to update an existing session with the given values.
func (s *Service) UpdateSession(id uuid.UUID, date *time.Time, duration float64, projectID *uuid.UUID) error {
	session, err := s.sessionStore.Get(id)
	if err != nil {
		return err
	}
	if session == nil {
		return fmt.Errorf("No Session found for the given id: %s", id)
	}
	if projectID != nil {
		p, err := s.projectStore.Get(*projectID)
		if err != nil {
			return err
		}
		if p == nil {
			return fmt.Errorf("The project with the id '%s' could not be found. You can only create sessions for existing projects", projectID)
		}
		session.ProjectID = *projectID
	}
	if duration > 0 {
		session.Duration = duration
	}
	if date != nil {
		session.Date = *date
	}
	err = s.sessionStore.Update(session)
	return err
}

// FindSessionsByDate searches for sessions within a selecte time bubble.
// In any case an array will be returned wether it is empty or filled with hits.
func (s *Service) FindSessionsByDate(from time.Time, to time.Time) ([]*VSession, error) {
	if from.After(to) {
		return nil, fmt.Errorf("From date is behind to date")
	}
	sessions, err := s.sessionStore.FindByDate(from, to)
	if err != nil {
		return nil, err
	}
	return sessions, nil
}

// FindSessionsByDateWithProject searches for sessions within a selecte time bubble filtered by the project name.
// In any case an array will be returned wether it is empty or filled with hits.
func (s *Service) FindSessionsByDateWithProject(from time.Time, to time.Time, name string) ([]*VSession, error) {
	if from.After(to) {
		return nil, fmt.Errorf("From date is behind to date")
	}

	sessions, err := s.sessionStore.FindByDateWithProject(from, to, name)
	if err != nil {
		return nil, err
	}
	return sessions, nil
}

// DeleteSession will delete the selected session.
func (s *Service) DeleteSession(id uuid.UUID) error {
	return s.sessionStore.Delete(id)
}
