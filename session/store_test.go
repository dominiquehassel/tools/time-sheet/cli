package session_test

import (
	"fmt"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

func TestCreateNewPgStore(t *testing.T) {
	//prepare
	db, _, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	// perform
	s := session.NewPgStore(db)
	// assert
	assert.NotNil(t, s)
}
func TestSaveSession(t *testing.T) {
	// prepare
	db, sqlMock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	sqlMock.ExpectBegin()
	d, _ := time.Parse("01-02-2006", "01-01-2020")
	s, _ := session.New(d, 4.5, uuid.New())
	sqlMock.ExpectExec("INSERT INTO session").WithArgs(s.ID, s.Date, s.Duration, s.ProjectID).WillReturnResult(sqlmock.NewResult(1, 1))
	sqlMock.ExpectCommit()
	store := session.NewPgStore(db)
	//perform
	err = store.Save(s)
	// assert
	assert.NoError(t, err)
	assert.NoError(t, sqlMock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestSaveSessionWithError(t *testing.T) {
	// prepare
	db, sqlMock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	sqlMock.ExpectBegin()
	d, _ := time.Parse("01-02-2006", "01-01-2020")
	s, _ := session.New(d, 4.5, uuid.New())
	sqlMock.ExpectExec("INSERT INTO session").WithArgs(s.ID, s.Date, s.Duration, s.ProjectID).WillReturnError(fmt.Errorf("Some Error"))
	sqlMock.ExpectRollback()
	store := session.NewPgStore(db)
	//perform
	err = store.Save(s)
	// assert
	assert.EqualError(t, err, "Some Error")
	assert.NoError(t, sqlMock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestUpdateSession(t *testing.T) {
	// prepare
	db, sqlMock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	sqlMock.ExpectBegin()
	d, _ := time.Parse("01-02-2006", "01-01-2020")
	s, _ := session.New(d, 4.5, uuid.New())
	sqlMock.ExpectExec(regexp.QuoteMeta("UPDATE session_table set sDate=$1, duration=$2, project=$3 where id=$4")).WithArgs(s.Date, s.Duration, s.ProjectID, s.ID).WillReturnResult(sqlmock.NewResult(1, 1))
	sqlMock.ExpectCommit()
	store := session.NewPgStore(db)
	//perform
	err = store.Update(s)
	// assert
	assert.NoError(t, err)
	assert.NoError(t, sqlMock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestGetExistingSession(t *testing.T) {
	// prepare
	db, sqlMock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	sqlMock.ExpectBegin()
	d, _ := time.Parse("01-02-2006", "01-01-2020")
	s, _ := session.New(d, 4.5, uuid.New())
	mockedRows := sqlmock.NewRows([]string{"id", "date", "duration", "project"})
	mockedRows.AddRow(s.ID, s.Date, s.Duration, s.ProjectID)
	sqlMock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM session_table where id=$1")).WithArgs(s.ID).WillReturnRows(mockedRows)
	sqlMock.ExpectCommit()
	store := session.NewPgStore(db)
	//perform
	ns, err := store.Get(s.ID)
	// assert
	assert.NoError(t, err)
	assert.Equal(t, s, ns)
	assert.NoError(t, sqlMock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestGetSessionNotFound(t *testing.T) {
	// prepare
	db, sqlMock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	id := uuid.New()
	mockedRows := sqlmock.NewRows([]string{"id", "date", "duration", "project"})
	sqlMock.ExpectBegin()
	sqlMock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM session_table where id=$1")).WithArgs(id).WillReturnRows(mockedRows)
	sqlMock.ExpectCommit()
	// perform
	s := session.NewPgStore(db)
	r, err := s.Get(id)
	// assert
	assert.NoError(t, err)
	assert.Nil(t, r, "Should be empty")
	assert.NoError(t, sqlMock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestFindSessionsWithinADate(t *testing.T) {
	// prepare
	db, sqlMock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	sqlMock.ExpectBegin()
	d1, _ := time.Parse("01-02-2006", "31-12-2019")
	d2, _ := time.Parse("01-02-2006", "02-01-2020")
	d, _ := time.Parse("01-02-2006", "01-01-2020")
	s1, _ := session.New(d, 4.5, uuid.New())
	s2, _ := session.New(d, 2.5, uuid.New())
	mockedRows := sqlmock.NewRows([]string{"id", "date", "duration", "project"})
	mockedRows.AddRow(s1.ID, s1.Date, s1.Duration, s1.ProjectID)
	mockedRows.AddRow(s2.ID, s2.Date, s2.Duration, s2.ProjectID)
	sqlMock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM session_view where Date>=$1 and Date<=$2")).WithArgs(d1, d2).WillReturnRows(mockedRows)
	sqlMock.ExpectCommit()
	store := session.NewPgStore(db)
	//perform
	l, err := store.FindByDate(d1, d2)
	// assert
	assert.NoError(t, err)
	assert.NotEmpty(t, l)
	assert.Equal(t, 2, len(l))
	assert.NoError(t, sqlMock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestFindSessionsForASelectedProjectInATimeBubble(t *testing.T) {
	// prepare
	db, sqlMock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	sqlMock.ExpectBegin()
	id := uuid.New()
	name := "test"
	d1, _ := time.Parse("01-02-2006", "31-12-2019")
	d2, _ := time.Parse("01-02-2006", "02-01-2020")
	d, _ := time.Parse("01-02-2006", "01-01-2020")
	s1, _ := session.New(d, 4.5, id)
	s2, _ := session.New(d, 2.5, id)
	mockedRows := sqlmock.NewRows([]string{"id", "date", "duration", "project"})
	mockedRows.AddRow(s1.ID, s1.Date, s1.Duration, name)
	mockedRows.AddRow(s2.ID, s2.Date, s2.Duration, name)
	sqlMock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM session_view where Project_Name=$1 and Date>=$2 and Date<=$3")).WithArgs(name, d1, d2).WillReturnRows(mockedRows)
	sqlMock.ExpectCommit()
	store := session.NewPgStore(db)
	//perform
	l, err := store.FindByDateWithProject(d1, d2, name)
	// assert
	assert.NoError(t, err)
	assert.NotEmpty(t, l)
	assert.Equal(t, 2, len(l))
	assert.NoError(t, sqlMock.ExpectationsWereMet(), "Mocking expectation should have met")
}

func TestDeleteExistingProject(t *testing.T) {
	// prepare
	db, mock, err := sqlmock.New()
	assert.NoError(t, err, "an error '%s' was not expected when opening a stub database connection", err)
	defer db.Close()
	id := uuid.New()
	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta("DELETE FROM session_table where id=$1")).WithArgs(id).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()
	// perform
	s := session.NewPgStore(db)
	err = s.Delete(id)
	// assert
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet(), "Mocking expectation should have met")
}
