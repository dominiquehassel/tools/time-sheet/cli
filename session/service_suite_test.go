package session_test

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/migration"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

type sessionSuiteTest struct {
	migration.PostgresqlSuite
}

func TestSessionSuite(t *testing.T) {

	dsn := os.Getenv("POSTGRES_SQL_TEST_URL")
	if dsn == "" {
		dsn = fmt.Sprintf("postgres://%s:%s@%s:%d/postgres?sslmode=disable", migration.User, migration.Password, migration.Host, migration.Port)
		/*fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbName)*/
	}

	projectSuite := &sessionSuiteTest{
		migration.PostgresqlSuite{
			DSN:                     dsn,
			MigrationLocationFolder: "../migration/data",
		},
	}

	suite.Run(t, projectSuite)
}

func (s *sessionSuiteTest) createServices() (*project.Service, *session.Service) {
	projectStore := project.NewPgStore(s.DBConn)
	projectService := project.NewService(projectStore)
	sessionStore := session.NewPgStore(s.DBConn)
	service := session.NewService(sessionStore, projectStore)
	return projectService, service
}

func (s *sessionSuiteTest) TestAddingSession() {

	// run Test Suite
	s.T().Run("should add a new project", func(t *testing.T) {
		// prepare
		projectService, service := s.createServices()
		date := time.Now()
		// perform
		p, _ := projectService.AddProject("test", "test", "test")
		s, err := service.AddSession(date, 5.5, p.ID)
		// assert
		assert.NoError(t, err)
		assert.Equal(t, 5.5, s.Duration)
	})
}

func (s *sessionSuiteTest) TestUpdateExistingSession() {

	// run Test Suite
	s.T().Run("should update an existing session", func(t *testing.T) {
		// prepare
		projectService, service := s.createServices()
		date := time.Now()
		// perform
		p, _ := projectService.AddProject("test", "test", "test")
		s, _ := service.AddSession(date, 5.5, p.ID)
		err := service.UpdateSession(s.ID, nil, 4.5, nil)
		services, _ := service.FindSessionsByDate(date, date)
		// assert
		assert.NoError(t, err)
		assert.Equal(t, 1, len(services))
		assert.Equal(t, 4.5, services[0].Duration)
	})
}

func (s *sessionSuiteTest) TestFindSessionsByDateWithAllTwoResults() {
	// run Test Suite
	s.T().Run("should find all two sessions ", func(t *testing.T) {
		// prepare
		projectService, service := s.createServices()
		date := time.Now()
		// perform
		p, _ := projectService.AddProject("test", "test", "test")
		service.AddSession(date, 5.5, p.ID)
		service.AddSession(date, 3.25, p.ID)
		services, err := service.FindSessionsByDate(date, date)
		// assert
		assert.NoError(t, err)
		assert.Equal(t, 2, len(services))
		assert.Equal(t, 5.5, services[0].Duration)
		assert.Equal(t, 3.25, services[1].Duration)
	})

}
func (s *sessionSuiteTest) TestFindSessionsByDateWithOnlyTwoResults() {
	// run Test Suite
	s.T().Run("should find only two", func(t *testing.T) {
		// prepare
		projectService, service := s.createServices()
		from, _ := time.Parse("02-01-2006", "15-06-2020")
		date, _ := time.Parse("02-01-2006", "15-11-2020")
		to := time.Now()
		past, _ := time.Parse("02-01-2006", "01-01-2020")
		// perform
		p, _ := projectService.AddProject("test", "test", "test")
		service.AddSession(past, 5.5, p.ID)
		service.AddSession(date, 5.5, p.ID)
		service.AddSession(date, 3.25, p.ID)
		services, err := service.FindSessionsByDate(from, to)
		// assert
		assert.NoError(t, err)
		assert.Equal(t, 2, len(services))
		assert.Equal(t, 5.5, services[0].Duration)
		assert.Equal(t, 3.25, services[1].Duration)
	})
}

func (s *sessionSuiteTest) TestFindSessionsByDateWithProjectWithOnlyTwoResults() {
	// run Test Suite
	s.T().Run("should find only two", func(t *testing.T) {
		// prepare
		projectService, service := s.createServices()
		from, _ := time.Parse("02-01-2006", "15-06-2020")
		date, _ := time.Parse("02-01-2006", "15-11-2020")
		to := time.Now()
		past, _ := time.Parse("02-01-2006", "01-01-2020")
		// perform
		name := "test"
		p, _ := projectService.AddProject(name, "test", "test")
		service.AddSession(past, 5.5, p.ID)
		service.AddSession(date, 5.5, p.ID)
		service.AddSession(date, 3.25, p.ID)
		services, err := service.FindSessionsByDateWithProject(from, to, name)
		// assert
		assert.NoError(t, err)
		assert.Equal(t, 2, len(services))
		assert.Equal(t, 5.5, services[0].Duration)
		assert.Equal(t, 3.25, services[1].Duration)
	})
}

func (s *sessionSuiteTest) TestDelteNonExistingSession() {
	// run Test Suite
	s.T().Run("should do nothing", func(t *testing.T) {
		// prepare
		_, service := s.createServices()
		// perform
		err := service.DeleteSession(uuid.New())
		// assert
		assert.NoError(t, err)
	})
}
