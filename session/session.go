package session

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

// Session defines a time slot connected with a project
type Session struct {
	ID        uuid.UUID `json:"ID"`
	Date      time.Time `json:"date"`
	Duration  float64   `json:"duration"`
	ProjectID uuid.UUID `json:"projectId"`
}

// VSession is a version of session optimized for the view.
type VSession struct {
	ID       uuid.UUID `json:"ID"`
	Date     time.Time `json:"date"`
	Duration float64   `json:"duration"`
	Project  string    `json:"projectName"`
}

// New creates a new session, referencing the given project id.
func New(date time.Time, duration float64, projectID uuid.UUID) (*Session, error) {

	if duration <= 0 {
		return nil, fmt.Errorf("A session withour a duration does not make any sense")
	}
	return &Session{ID: uuid.New(), Date: date, Duration: duration, ProjectID: projectID}, nil
}

// NewVSession creates a new view session, referencing a given project.
func NewVSession(date time.Time, duration float64, project string) (*VSession, error) {

	if duration <= 0 {
		return nil, fmt.Errorf("A session withour a duration does not make any sense")
	}
	return &VSession{ID: uuid.New(), Date: date, Duration: duration, Project: project}, nil
}
