package session

import (
	"database/sql"
	"time"

	"github.com/google/uuid"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/helper"
)

// Store defines the interface actions for the persitance layer.
type Store interface {
	Save(*Session) error
	Update(*Session) error
	Get(uuid.UUID) (*Session, error)
	FindByDate(from time.Time, to time.Time) ([]*VSession, error)
	FindByDateWithProject(from time.Time, to time.Time, project string) ([]*VSession, error)
	Delete(uuid.UUID) error
}

// PgStore is an implementation of the #Store for Postgresql Database connections.
type PgStore struct {
	helper.DbStore
}

// NewPgStore creates a new Store with the given sql connection.
// This store is optimized for Postgresql DB
func NewPgStore(db *sql.DB) *PgStore {
	s := &PgStore{}
	s.DB = db
	return s
}

// Save `s a new session into the persistance layer.
func (s *PgStore) Save(session *Session) error {
	return s.Execute(func(tx *sql.Tx) error {
		if _, err := tx.Exec("INSERT INTO session_table (id, sDate, duration, project) VALUES ($1, $2, $3, $4)", session.ID, session.Date, session.Duration, session.ProjectID); err != nil {
			return err
		}
		return nil
	})
}

// Update `s an existing session with new values.
func (s *PgStore) Update(session *Session) error {
	return s.Execute(func(tx *sql.Tx) error {
		if _, err := tx.Exec("UPDATE session_table set sDate=$1, duration=$2, project=$3 where id=$4", session.Date, session.Duration, session.ProjectID, session.ID); err != nil {
			return err
		}
		return nil
	})
}

// Get searces for an existing session and returns it. If the session could not be found, nil will be returned.
func (s *PgStore) Get(id uuid.UUID) (*Session, error) {
	ses := &Session{}
	err := s.Execute(func(tx *sql.Tx) error {
		// execute the sql statement
		row := tx.QueryRow("SELECT * FROM session_table where id=$1", id)

		// unmarshal the row object to user
		err := row.Scan(&ses.ID, &ses.Date, &ses.Duration, &ses.ProjectID)
		if err == sql.ErrNoRows {
			ses = nil
			return nil
		} else if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return ses, nil
}

// FindByDate returns all session found in the selected time line
func (s *PgStore) FindByDate(from time.Time, to time.Time) ([]*VSession, error) {
	var sessions []*VSession
	err := s.Execute(func(tx *sql.Tx) error {
		// execute the sql statement
		rows, err := tx.Query("SELECT * FROM session_view where Date>=$1 and Date<=$2", from, to)
		if err != nil {
			return err
		}
		for rows.Next() {
			ses := &VSession{}
			err := rows.Scan(&ses.ID, &ses.Date, &ses.Duration, &ses.Project)
			if err == sql.ErrNoRows {
				return nil
			} else if err != nil {
				return err
			}
			sessions = append(sessions, ses)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return sessions, nil
}

// FindByDateWithProject returns all session found in the selected time line with a filter for project id
func (s *PgStore) FindByDateWithProject(from time.Time, to time.Time, project string) ([]*VSession, error) {
	var sessions []*VSession
	err := s.Execute(func(tx *sql.Tx) error {
		// execute the sql statement
		rows, err := tx.Query("SELECT * FROM session_view where Project_Name=$1 and Date>=$2 and Date<=$3", project, from, to)
		if err != nil {
			return err
		}
		for rows.Next() {
			ses := &VSession{}
			err := rows.Scan(&ses.ID, &ses.Date, &ses.Duration, &ses.Project)
			if err == sql.ErrNoRows {
				return nil
			} else if err != nil {
				return err
			}
			sessions = append(sessions, ses)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return sessions, nil
}

// Delete drops the entry  with the given id from the session table.
func (s *PgStore) Delete(id uuid.UUID) error {
	return s.Execute(func(tx *sql.Tx) error {
		if _, err := tx.Exec("DELETE FROM session_table where id=$1", id); err != nil {
			return err
		}
		return nil
	})
}
