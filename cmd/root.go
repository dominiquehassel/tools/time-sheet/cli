/*
Copyright © 2020 dominiquehassel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"

	"gitlab.com/dominiquehassel/tools/time-sheet/cli/mask"
)

var logger = logrus.New()

const key = "ThisIsTheDefaultKeyForMaskedPasswords"

var (
	psql                         = "postgresql"
	host, user, password, dbName string
	port                         int
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "ts",
	Short: "Manage your time-sheet with this small cli",
	Long: `Small cli to manage your time-sheet. 
Book your working time on project you previously added as references like this:

ts add project -n example-project -d "a small description" -b <a booking reference>

`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().BoolVarP(&debug, "verbose", "v", false, "Set verbose for output level.")
}

var debug bool

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	// Find home directory.
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Search config in home directory with name ".cmd" (without extension).
	path := filepath.Join(home, ".config")
	viper.AddConfigPath(path)
	viper.SetConfigType("yaml")
	viper.SetConfigName(".time-sheet")

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		logger.Debugf("Using config file: %s", viper.ConfigFileUsed())
	}

	if debug {
		logger.SetLevel(logrus.DebugLevel)
	}
}

func openConnection() *sql.DB {
	host = viper.GetString("db.host.name")
	port = viper.GetInt("db.host.port")
	user = viper.GetString("db.user")
	password = viper.GetString("db.password")
	dbName = viper.GetString("db.name")

	decryptedPassword, err := mask.Decrypt(password, key)

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", user, decryptedPassword, host, port, dbName)
	logger.Debugf("Open connection: %s", dsn)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		logger.Fatalf("Unexpected error : %s", err.Error())
	}

	logger.Debug("connected")
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	err = db.PingContext(ctx)
	if err != nil {
		logger.Fatalf("Errors %s pinging DB", err)
	}
	logger.Debug("connection tested successfully")
	return db
}
