/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os"

	"github.com/jedib0t/go-pretty/table"
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
)

// projectFindCmd represents the projectFind command
var projectFindCmd = &cobra.Command{
	Use:   "project",
	Short: "Searches for all projects",
	Run: func(cmd *cobra.Command, args []string) {
		name, err := cmd.Flags().GetString("name")
		if err != nil {
			logger.Fatalf("Unexpected Error: %s", err)
		}
		db := openConnection()
		store := project.NewPgStore(db)
		s := project.NewService(store)
		ps, err := s.FindProjects(name)
		if err != nil {
			logger.Fatalf("Unexpected Error: %s", err)
		}
		logger.Debugf("Found %d Project(s)", len(ps))

		printProjectTable(ps)
	},
}

func init() {
	findCmd.AddCommand(projectFindCmd)

	projectFindCmd.Flags().StringP("name", "n", "", "The name of the project to look for")
}

func printProjectTable(projects []*project.Project) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Name", "Booking Reference", "Description"})
	for _, p := range projects {
		t.AppendRow([]interface{}{p.ID, p.Name, p.BookingReference, p.Description})
	}
	t.Render()
}
