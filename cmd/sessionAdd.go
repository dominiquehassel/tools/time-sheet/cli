/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"time"

	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

// sessionCmd represents the session command
var sessionCmd = &cobra.Command{
	Use:   "session",
	Short: "Adds a new session to the time-sheet",

	Run: func(cmd *cobra.Command, args []string) {
		date, err := cmd.Flags().GetString("date")
		handleFatal(err)
		dateF, err := time.Parse("02-01-2006", date)
		handleFatal(err)
		duration, err := cmd.Flags().GetFloat64("duration")
		handleFatal(err)
		projectString, err := cmd.Flags().GetString("project")
		handleFatal(err)
		projectF, err := uuid.Parse(projectString)
		handleFatal(err)
		db := openConnection()
		s := session.NewService(session.NewPgStore(db), project.NewPgStore(db))
		newSession, err := s.AddSession(dateF, duration, projectF)
		handleFatal(err)
		logger.Infof("Session %s created", newSession.ID)
	},
}

func init() {
	addCmd.AddCommand(sessionCmd)

	sessionCmd.Flags().StringP("date", "d", time.Now().Format("02-01-2006"), "The date of the session")
	sessionCmd.Flags().Float64P("duration", "D", 0.5, "The duration of the session. Must be greater than 0.")
	sessionCmd.Flags().StringP("project", "p", "", "The referenced project. Must not be nil")
	sessionCmd.MarkFlagRequired("duration")
	sessionCmd.MarkFlagRequired("project")
}
