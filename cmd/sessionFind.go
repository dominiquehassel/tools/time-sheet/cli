/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os"
	"time"

	"github.com/jedib0t/go-pretty/table"
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

// sessionFindCmd represents the sessionFind command
var sessionFindCmd = &cobra.Command{
	Use:   "session",
	Short: "Searches for sessions in a dedicated time bubble or a dedicated project reference",
	Run: func(cmd *cobra.Command, args []string) {
		from, err := cmd.Flags().GetString("from")
		handleFatal(err)
		fromF, err := time.Parse("02-01-2006", from)
		to, err := cmd.Flags().GetString("to")
		handleFatal(err)
		toF, err := time.Parse("02-01-2006", to)
		projectName, err := cmd.Flags().GetString("project")
		handleFatal(err)
		db := openConnection()
		s := session.NewService(session.NewPgStore(db), project.NewPgStore(db))
		var sessions []*session.VSession
		logger.Debugf("Looking for sessions between %s and %s for the project %s", from, to, projectName)
		if projectName != "" {
			sessions, err = s.FindSessionsByDateWithProject(fromF, toF, projectName)
			handleFatal(err)
		} else {
			sessions, err = s.FindSessionsByDate(fromF, toF)
			handleFatal(err)
		}
		printSessionTable(sessions)
	},
}

func init() {
	findCmd.AddCommand(sessionFindCmd)

	sessionFindCmd.Flags().String("from", time.Now().Format("02-01-2006"), "The start date of the search intervall. Default is today")
	sessionFindCmd.Flags().String("to", time.Now().Format("02-01-2006"), "The end date of the search intervall. Default is today")
	sessionFindCmd.Flags().StringP("project", "p", "", "The name of the project to filter the result")
}

func printSessionTable(sessions []*session.VSession) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Date", "Duration", "Project Name"})
	for _, s := range sessions {
		t.AppendRow([]interface{}{s.ID, s.Date, s.Duration, s.Project})
	}
	t.Render()
}
