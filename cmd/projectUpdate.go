/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
)

// projectUpdateCmd represents the projectUpdate command
var projectUpdateCmd = &cobra.Command{
	Use:   "project",
	Short: "updates a selected project with a new Name, Description and/or BookingReference",
	Run: func(cmd *cobra.Command, args []string) {
		id, err := cmd.Flags().GetString("id")
		if err != nil {
			logger.Fatalf("Unexpected Error: %s", err)
		}
		uuid, err := uuid.Parse(id)
		if err != nil {
			logger.Fatalf("Unexpected Error while parsing uuid: %s", err)
		}
		name, err := cmd.Flags().GetString("name")
		if err != nil {
			logger.Fatalf("Unexpected Error: %s", err)
		}
		description, err := cmd.Flags().GetString("description")
		if err != nil {
			logger.Fatalf("Unexpected Error: %s", err)
		}
		bookingReference, err := cmd.Flags().GetString("bookingReference")
		if err != nil {
			logger.Fatalf("Unexpected Error: %s", err)
		}
		db := openConnection()
		store := project.NewPgStore(db)
		s := project.NewService(store)
		err = s.UpdateProject(uuid, name, description, bookingReference)
		if err != nil {
			logger.Fatalf("Unexpected Error when updating project: %s", err)
		}
	},
}

func init() {
	updateCmd.AddCommand(projectUpdateCmd)

	projectUpdateCmd.Flags().String( "id", "", "The id of the project to be updated")
	projectUpdateCmd.Flags().StringP("name", "n", "", "The new name of the Project. Must not be empty")
	projectUpdateCmd.Flags().StringP("description", "d", "", "The new description of the project. May be empty")
	projectUpdateCmd.Flags().StringP("bookingReference", "r", "", "The new booking reference of the project. Must not be empty")
}
