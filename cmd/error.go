package cmd

func handleFatal(err error) {
	if err != nil {
		logger.Fatalf("Unexpected Error: %s", err)
	}
}
