/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/migration"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initializes the database using migrations.",
	Run: func(cmd *cobra.Command, args []string) {
		path := "./migration/data"
		db := openConnection()
		defer func () {
			logger.Info("closing database connection")
			db.Close()
		}()
		m, err := migration.CreateMigration(db, path)
		if err !=nil {
			logger.Fatalf("Error while creating migrations: %s", err)
		}
		_, err = m.Up()
		if err != nil {
			logger.Fatalf("Unexpected Error while initializing the database: %s ", err)
		}
		logger.Info("Database successfully initialized")
	},
}

func init() {
	configCmd.AddCommand(initCmd)
}
