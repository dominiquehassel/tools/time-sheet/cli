/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

// sessionDeleteCmd represents the sessionDelete command
var sessionDeleteCmd = &cobra.Command{
	Use:   "session",
	Short: "Deletes an existing session",

	Run: func(cmd *cobra.Command, args []string) {
		id, err := cmd.Flags().GetString("id")
		handleFatal(err)
		uuid, err := uuid.Parse(id)
		handleFatal(err)
		db := openConnection()
		s := session.NewService(session.NewPgStore(db), project.NewPgStore(db))
		err = s.DeleteSession(uuid)
		handleFatal(err)
	},
}

func init() {
	deleteCmd.AddCommand(sessionDeleteCmd)

	sessionDeleteCmd.Flags().String("id", "", "The id of the project to be deleted")
}
