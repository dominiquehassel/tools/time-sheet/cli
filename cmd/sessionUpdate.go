/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"time"

	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/project"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/session"
)

// sessionUpdateCmd represents the sessionUpdate command
var sessionUpdateCmd = &cobra.Command{
	Use:   "session",
	Short: "Updates an existing session with date duration of project reference",
	Run: func(cmd *cobra.Command, args []string) {
		id, err := cmd.Flags().GetString("id")
		handleFatal(err)
		idF, err := uuid.Parse(id)
		handleFatal(err)
		date, err := cmd.Flags().GetString("date")
		handleFatal(err)
		var dateF *time.Time
		if date != "" {
			pd, err := time.Parse("02-01-2006", date)
			handleFatal(err)
			dateF = &pd
		}
		duration, err := cmd.Flags().GetFloat64("duration")
		handleFatal(err)
		projectString, err := cmd.Flags().GetString("project")
		handleFatal(err)
		var projectF *uuid.UUID
		if projectString != "" {
			pf, err := uuid.Parse(projectString)
			handleFatal(err)
			projectF = &pf
		}
		db := openConnection()
		s := session.NewService(session.NewPgStore(db), project.NewPgStore(db))
		err = s.UpdateSession(idF, dateF, duration, projectF)
		handleFatal(err)
		logger.Infof("Session %s successfully updated", id)
	},
}

func init() {
	updateCmd.AddCommand(sessionUpdateCmd)

	sessionUpdateCmd.Flags().String("id", "", "The id of the session to be updated")
	sessionUpdateCmd.Flags().StringP("date", "d", time.Now().Format("02-01-2006"), "The date of the session")
	sessionUpdateCmd.Flags().Float64P("duration", "D", 0.5, "The duration of the session. Must be greater than 0.")
	sessionUpdateCmd.Flags().StringP("project", "p", "", "The referenced project. Must not be nil")
}
