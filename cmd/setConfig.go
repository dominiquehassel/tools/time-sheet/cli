/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/dominiquehassel/tools/time-sheet/cli/mask"
)

// setConfigCmd represents the setConfig command
var setConfigCmd = &cobra.Command{
	Use:   "set",
	Short: "Possibility to set/reset selected configuration parameters for time-sheet cli",
	Run: func(cmd *cobra.Command, args []string) {
		encryptedPassword, err := mask.Encrypt(password, key)
		if err != nil {
			logger.Fatalf("Unexpected Error while encoding password: %s", err)
		}
		viper.Set("db.host.name", host)
		viper.Set("db.host.port", port)
		viper.Set("db.user", user)
		viper.Set("db.password", encryptedPassword)
		viper.Set("db.dbName", dbName)
		err = viper.WriteConfig()
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			viper.SafeWriteConfig()
		} else if err != nil {
			logger.Fatalf("Unexpected error while writing config: %s", err)
		}
	},
}

func init() {
	configCmd.AddCommand(setConfigCmd)

	setConfigCmd.Flags().StringVarP(&password,"password", "p", "", "Password in clear text. Will be encrypted to be stored in the configuration file.")
	setConfigCmd.Flags().StringVarP(&user,"user", "u", "", "User name for the database connection.")
	setConfigCmd.Flags().StringVarP(&host,"host", "H", "", "Host name for the database connection.")
	setConfigCmd.Flags().IntVarP(&port,"port", "P", 5432, "Port number for the database connection. The default is 5432 (Standart for Postgresql) ")
	setConfigCmd.Flags().StringVarP(&dbName,"db-name", "n", "", "Name of the database to connect to.")
}
