package mask_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/dominiquehassel/tools/time-sheet/cli/mask"
)

func TestCoverPasswords(t *testing.T) {
	tt := map[string]struct {
		key      string
		original string
		errMsg   string
	}{
		"test encrypt string working": {
			key:      "test",
			original: "foo",
			errMsg:   "",
		},
		"test encrypt with empty key": {
			key:      "",
			original: "foo",
		},
		"test with data with more than 16 characters": {
			key:      "test",
			original: "This data has more than 16 characters",
		},
		"test with empty data": {
			key:      "test",
			original: "",
		},
		"test with key longer than 16 characters": {
			key:      "key longer than 16 characters",
			original: "foo",
		},
		"test with special caracters in data": {
			key:      "test",
			original: "?CI?8sb,2[%&u(3",
		},
		"test with special characters in key": {
			key:      "?CI?8sb,2[%&u(3",
			original: "foo",
		},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			covered, err := mask.Encrypt(tc.original, tc.key)
			assert.NoError(t, err)
			result, err := mask.Decrypt(covered, tc.key)
			assert.NoError(t, err)
			assert.Equal(t, tc.original, result)
		})
	}
}
