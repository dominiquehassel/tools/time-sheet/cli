package migration

import (
	"database/sql"
	"strings"

	_ "github.com/lib/pq"

	"github.com/golang-migrate/migrate/v4"
	_psql "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

// Migration hold the database migration object to perform up/down on it.
type Migration struct {
	migrate *migrate.Migrate
}

// Runns all migration up scripts found in the selected folder
func (m *Migration) Up() (bool, error) {
	err := m.migrate.Up()
	if err != nil {
		if err == migrate.ErrNoChange {
			return true, nil
		}
		return false, err
	}
	return true, nil
}
// Runns all migration down scripts found in the selected folder
func (m *Migration) Down() (bool, error) {
	err := m.migrate.Down()
	if err != nil {
		return false, err
	}
	return true, nil
}

// CreateMigration new migrations and connects to the selected database.
func CreateMigration(dbConn *sql.DB, migrationsFolderLocation string) (*Migration, error) {
	dataPath := []string{}
	dataPath = append(dataPath, "file://")
	dataPath = append(dataPath, migrationsFolderLocation)

	pathToMigrate := strings.Join(dataPath, "")

	driver, err := _psql.WithInstance(dbConn, &_psql.Config{})
	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithDatabaseInstance(pathToMigrate, "postgres", driver)
	if err != nil {
		return nil, err
	}

	//m.Force(0)
	return &Migration{migrate: m}, nil
}
