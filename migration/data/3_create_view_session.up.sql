CREATE VIEW session_view AS
SELECT 
    s.id AS Session_ID, 
    s.sDate AS Date, 
    s.duration AS Duration,
    p.projectName AS Project_Name
FROM 
    session_table s,
    project p
WHERE 
    s.project = p.id;