CREATE TABLE IF NOT EXISTS project (
    id uuid PRIMARY KEY,
    projectName VARCHAR,
    projectDescription VARCHAR,
    bookingReference VARCHAR
)