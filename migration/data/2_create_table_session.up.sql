CREATE TABLE IF NOT EXISTS session_table (
    id uuid PRIMARY KEY,
    sDate TIMESTAMP,
    duration VARCHAR,
    project uuid REFERENCES project(id)
)