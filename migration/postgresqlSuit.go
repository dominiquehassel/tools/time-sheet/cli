package migration

import (
	"database/sql"
	"io"
	"log"

	"github.com/stretchr/testify/suite"

	_ "github.com/golang-migrate/migrate/v4/source/file"

	//using only postgresql

	"github.com/stretchr/testify/require"
)

const (
	Psql     = "postgres"
	Host     = "localhost"
	Port     = 5432
	User     = "postgres"
	Password = "changeme"
	DbName   = "postgres"
)

type PostgresqlSuite struct {
	suite.Suite
	DSN                     string
	DBConn                  *sql.DB
	Migration               *Migration
	MigrationLocationFolder string
	DBName                  string
}

func (s *PostgresqlSuite) SetupTest() {
	log.Println("Starting a Test. Migrating the Database")
	_, err := s.Migration.Up()
	require.NoError(s.T(), err)
	log.Println("Database Migrated Successfully")
}

func (s *PostgresqlSuite) TearDownTest() {
	log.Println("Finishing Test. Dropping The Database")
	_, err := s.Migration.Down()
	require.NoError(s.T(), err)
	log.Println("Database Dropped Successfully")
}

// https://blevesearch.com/news/Deferred-Cleanup,-Checking-Errors,-and-Potential-Problems/
func Close(c io.Closer) {
	err := c.Close()
	if err != nil {
		log.Fatal(err)
	}
}

// SetupSuite setup at the beginning of test
func (s *PostgresqlSuite) SetupSuite() {
	//DisableLogging()

	var err error

	s.DBConn, err = sql.Open(Psql, s.DSN)
	for {
		err := s.DBConn.Ping()
		if err == nil {
			break
		}
	}

	s.Migration, err = CreateMigration(s.DBConn, s.MigrationLocationFolder)
	require.NoError(s.T(), err)

}

// TearDownSuite teardown at the end of test
func (s *PostgresqlSuite) TearDownSuite() {
	s.DBConn.Close()
}
